use pyo3::prelude::*;
use pyo3::wrap_pymodule;

pub mod py_environment;
pub mod py_ir;
pub mod py_layerwise;
pub mod py_utils;

use crate::api::py::py_ir::*;
use crate::api::py::py_layerwise::*;
use crate::api::py::py_utils::*;

#[pymodule]
fn HPGO(_py: Python<'_>, m: &PyModule) -> PyResult<()> {
    m.add("__title__", "HPGO")?;
    m.add("__package__", "HPGO")?;
    m.add("__doc__", "Hybrid Parallelism Global Orchestration")?;
    m.add("__author__", "Yi Rong <hi@rongyi.io>")?;
    m.add("__copyright__", "Copyright 2020 Yi Rong")?;
    m.add("__license__", "BSD-3-Clause")?;
    m.add("__version__", env!("CARGO_PKG_VERSION"))?;
    m.add_wrapped(wrap_pymodule!(Layerwise))?;
    m.add_wrapped(wrap_pymodule!(IR))?;
    m.add_wrapped(wrap_pymodule!(Utils))?;

    Ok(())
}
