module github.com/LER0ever/HPGO/src/input/hlo-parser

go 1.13

require (
	github.com/alecthomas/participle v0.4.1
	github.com/sirupsen/logrus v1.4.2
	github.com/tidwall/pretty v1.0.1
)
